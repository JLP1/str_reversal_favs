/* -----------------------------------------------------------------
     A P P L I E D   M I N D S :   S T R I N G   R E V E R S A L
 * -----------------------------------------------------------------
 * @ JLP : 2014-02-27 
 *
 * Purpose: Demonstrate string reversal process with Angular js UI.
 *
 */


    /* - GOOGLE API KEY: -

    API key AIzaSyAm_UJFxIXcMN1jRL_PNFPCypKwxehU9LQ
    Referers    
    http://palisade.jlp1.com/reversal/
    Activation date Mar 1, 2014 5:24 AM
    Activated by     jlp.boston@gmail.com (you)
    */


/*  Create Angular Js Appliction: */
var stringplay = angular.module( 'stringplay', [] );
 
/* -------------------------------
 *  Angular controller:
 * -------------------------------
*/
stringplay.controller('stringController', function ($scope , $http) {

    // Each theme name matches a CSS selector class, e.g.: .theme-ocean.
    $scope.themes = [
        { name: "default", id: 1 },
        { name: "ocean",   id: 2 },
        { name: "tech",    id: 3 },
        { name: "kyoto",   id: 4 },
        { name: "orange",  id: 5 },
        { name: "large",   id: 6 }
    ];

    $scope.selectedTheme = $scope.themes[0];

    // mirror
    $scope.mirror = "symmetry";

    // pattern art gen
    // constrain system / art pallet

    // define pattern styles for art generation
    $scope.pattern_styles = [

        { style: 'Alpha', 
          chars: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"},
        
        { style: 'Numbers', 
          chars: "0123456789"}, 

        { style: 'Space', 
          chars: "                      "},
                
         { style: 'Dots',   
          chars: ".,:;'`~*·♦◊"},

         { style: 'Lines',   
          chars: "-_=|/"},

        { style: 'Special', 
          chars: "!@#$%^&*()-_+={}[]|/?><`~.," },
        
        { style: 'Arrows',   
          chars: "‹›«»∇Δ↓↑←→⇔"},
    ];

    var lookup = {};
    for (var i = 0, len = $scope.pattern_styles.length; i < len; i++) {
        lookup[$scope.pattern_styles[i].style] = $scope.pattern_styles[i];
    }

    $scope.pattern_lookup = lookup;

    // selected styles
    $scope.selection = ['Space', 'Arrows', 'Lines', 'Dots'];

    // toggle selection for a given style by name
    $scope.toggleSelection = function toggleSelection(style) {
        
        var idx = $scope.selection.indexOf(style);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(style);
        }
    };

    // Length of the generated string
    $scope.gen_len = 20;

    // step of the spinner (controlled via the slider)
    $scope.gen_len_step_attr = 3;

    // generate pattern
    $scope.patternGen = function() {
        var text = "",
            style = "",
            char_pool = "",
            char_pool_length = 0,
            gen_len = $scope.gen_len,
            i = 0;

        // Create combined list of chars based on what the user
        // selected in the UI via the checkboxes.
        for (i=0; i<$scope.selection.length; i++) {
            style = $scope.selection[i];
            char_pool += $scope.pattern_lookup[ style ].chars;    
        }

        char_pool_length = char_pool.length;

        for (i=0; i<gen_len; i++) {
            text += char_pool.charAt(Math.floor(Math.random() * char_pool_length));
        }

        $scope.stringInput = text;
    };

    // ----------------------- RESEARCH ----------------------


    $scope.url = '../cgi-bin/demo.cgi'; // set url for research gathering

   // $scope.url = "https://www.googleapis.com/customsearch/v1?q=reverse+a+string&cx=012709690560969926980%3Arz59qipfo4c&key=AIzaSyAm_UJFxIXcMN1jRL_PNFPCypKwxehU9LQ&callback=hndlr";

    /* http call to get research data from web */
    $scope.loadResearch = function() {
        
        console.log('* getting research online *');
        
        $http.post($scope.url,'demo').
        success(function(data, status) {
            
            /* process json data */     
            $scope.status = status;
            $scope.data = data;
            $scope.result = data;
            $scope.show_answers_title = 'Answers provided by Angular http call to JSON.';

            /* create array from data */
            /**/
            if ( angular.isArray(data.research) ) {
                $scope.research = data.research;
            }
            else {
                $scope.reports = [data.research];
            }
            
            /*google
            if ( angular.isArray(data.items) ) {
                $scope.research = data.items;
            }
            else {
                $scope.research = [data.items];
            }*/


        })
        .
        error(function(data, status) {
            $scope.data = data || "Request failed";
            $scope.status = status;         
        });
    };
    // close http request

    $scope.input_strings = []

    /* set initial favorites */
    $scope.fav_strings = [
        /* Example Schema:
             {'string': 'test',
             'fascination': 'testing'},
            {'string': 'boston',
             'fascination': 'city in spring.'}
         */
    ];

    $scope.keydown = function() {
    /* on key press, handle string reversal */
        var str_reverse = backwards($scope.stringInput);
        var str_design ;

        /* set the current reversed string */
        $scope.magic_string = str_reverse;

        if( $scope.mirror == 'symmetry'){

            // combine
            str_design = str_reverse + $scope.stringInput;

        } else {

            str_design = str_reverse;
        }


        /* capture each input string, use for frontend readout */
        $scope.input_strings.unshift({
            string: str_design
        });

        /* output to console */
        console.log( str_reverse );
    };

    $scope.stringSwap = function(magic_string) {
        $scope.stringInput = magic_string;
    }


   $scope.addFav = function( magic_string ) {
   /* save reversed string as a favorite */

        /* first save the last favorite to our favorites history */
        if( $scope.favorite ){
            /* count string chars */ 
            var slen = $scope.favorite.length;

            /* prepend to favorites array */
            $scope.fav_strings.unshift({
                string: $scope.favorite,
                fascination: '..excellent.. ' + slen + ' characters.'
            });
        }
        
        /* now set the favorite to the latest selection */
        console.log( '> Your new favorite string is: ' + magic_string );
        $scope.favorite = magic_string;

    }

    /* 
    $scope.loadString = function( input_str ) {
        $scope.input_string = input_str;
    }
    */

});
/* close controller */

/* -------------------------------
 *  Reverse String:
 * -------------------------------
*/
function backwards( str ) {
    
    /* get the position of the last character of the string */
    var index_last = str.length - 1;

    var new_str = ""; 
    
    /* initialize index for loop at last character,
     * decrement during iteration 
     */
    for ( i=index_last; i>=0; --i ) {

        /* append character-at-index to newstring */        
        new_str += str[i];
    }
    return new_str;
}

